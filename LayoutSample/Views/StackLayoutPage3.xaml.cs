﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace LayoutSample.Views
{
    /// <summary>
    /// 一番上のコンボボックスを選択することでStackOrientationが変わります。
    /// </summary>
    public partial class StackLayoutPage3 : ContentPage
    {
        readonly Dictionary<string, StackOrientation> pickerDic = new Dictionary<string, StackOrientation>()
        {
            { "Vertical(垂直方向)", StackOrientation.Vertical },
            { "Horizontal(水平方向)", StackOrientation.Horizontal }
        };

        public StackLayoutPage3()
        {
            InitializeComponent();
            foreach (string key in pickerDic.Keys)
            {
                this.picker.Items.Add(key);
            }
        }

        void Picker_SelectedIndexChanged(System.Object sender, System.EventArgs e)
        {
            string key = picker.Items[picker.SelectedIndex];
            this.stack.Orientation = this.pickerDic[key];
        }
    }
}
