﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace LayoutSample.Views
{
    /// <summary>
    /// 垂直方向レイアウトのサンプル
    /// </summary>
    public partial class StackLayoutPage2 : ContentPage
    {

        public StackLayoutPage2()
        {
            InitializeComponent();
        }
    }
}
