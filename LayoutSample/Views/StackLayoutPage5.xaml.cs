﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace LayoutSample.Views
{
    /// <summary>
    /// ボタンをクリックするとVerticalOptionsが変わります
    /// </summary>
    public partial class StackLayoutPage5 : ContentPage
    {

        public StackLayoutPage5()
        {
            InitializeComponent();
        }

        void button1_Clicked(System.Object sender, System.EventArgs e)
        {
            this.button1.VerticalOptions = LayoutOptions.StartAndExpand;
            this.button1.Text = "StartAndExpand";
            this.button2.VerticalOptions = LayoutOptions.Start;
            this.button2.Text = "Start";
            this.button3.VerticalOptions = LayoutOptions.Start;
            this.button3.Text = "Start";
        }

        void button2_Clicked(System.Object sender, System.EventArgs e)
        {
            this.button1.VerticalOptions = LayoutOptions.Center;
            this.button1.Text = "Center";
            this.button2.VerticalOptions = LayoutOptions.CenterAndExpand;
            this.button2.Text = "CenterAndExpand";
            this.button3.VerticalOptions = LayoutOptions.Center;
            this.button3.Text = "Center";
        }

        void button3_Clicked(System.Object sender, System.EventArgs e)
        {
            this.button1.VerticalOptions = LayoutOptions.End;
            this.button1.Text = "End";
            this.button2.VerticalOptions = LayoutOptions.End;
            this.button2.Text = "End";
            this.button3.VerticalOptions = LayoutOptions.EndAndExpand;
            this.button3.Text = "EndAndExpand";
        }
    }
}
