﻿using System.ComponentModel;
using LayoutSample.Views;
using Xamarin.Forms;

namespace LayoutSample
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {

        public MainPage()
        {
            InitializeComponent();
        }

        /// <summary>
        /// StackLayoutボタン押下時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ButStackLayout_Clicked(System.Object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new StackLayoutPage());
        }

        void ButStackLayout2_Clicked(System.Object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new StackLayoutPage2());
        }

        void ButStackLayout3_Clicked(System.Object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new StackLayoutPage3());
        }

        void ButStackLayout4_Clicked(System.Object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new StackLayoutPage4());
        }

        void ButStackLayout5_Clicked(System.Object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new StackLayoutPage5());
        }
    }
}
