﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace LayoutSample.Views
{
    /// <summary>
    /// 水平方向レイアウトのサンプル
    /// </summary>
    public partial class StackLayoutPage : ContentPage
    {

        public StackLayoutPage()
        {
            InitializeComponent();
        }
    }
}
