﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace LayoutSample.Views
{
    public partial class StackLayoutPage4 : ContentPage
    {
        /// <summary>
        /// 各ボタンをクリックすると、HorizontalOptionsが変わります。
        /// </summary>
        public StackLayoutPage4()
        {
            InitializeComponent();
        }

        void button1_Clicked(System.Object sender, System.EventArgs e)
        {
            this.button1.HorizontalOptions = LayoutOptions.StartAndExpand;
            this.button1.Text = "StartAndExpand";
            this.button2.HorizontalOptions = LayoutOptions.Start;
            this.button2.Text = "Start";
            this.button3.HorizontalOptions = LayoutOptions.Start;
            this.button3.Text = "Start";
        }

        void button2_Clicked(System.Object sender, System.EventArgs e)
        {
            this.button1.HorizontalOptions = LayoutOptions.Center;
            this.button1.Text = "Center";
            this.button2.HorizontalOptions = LayoutOptions.CenterAndExpand;
            this.button2.Text = "CenterAndExpand";
            this.button3.HorizontalOptions = LayoutOptions.Center;
            this.button3.Text = "Center";
        }

        void button3_Clicked(System.Object sender, System.EventArgs e)
        {
            this.button1.HorizontalOptions = LayoutOptions.End;
            this.button1.Text = "End";
            this.button2.HorizontalOptions = LayoutOptions.End;
            this.button2.Text = "End";
            this.button3.HorizontalOptions = LayoutOptions.EndAndExpand;
            this.button3.Text = "EndAndExpand";
        }
    }
}
